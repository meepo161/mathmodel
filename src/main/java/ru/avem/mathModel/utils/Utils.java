package ru.avem.mathModel.utils;

public class Utils {
    public static void sleep(long mills) {
        if (mills > 0) {
            try {
                Thread.sleep(mills);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
