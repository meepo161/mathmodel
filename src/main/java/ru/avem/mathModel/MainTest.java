package ru.avem.mathModel;

import ru.avem.mathModel.model.MathModelResonance;
import ru.avem.mathModel.model.Signal;
import ru.avem.mathModel.utils.Utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MainTest {
    public static void main(String[] args) {
//        MathModelResonance testModel = new MathModelResonance(Signal.PERFECT);
//        MathModelResonance testModel = new MathModelResonance(Signal.GOOD);
//        MathModelResonance testModel = new MathModelResonance(Signal.BAD);
        MathModelResonance testModel = new MathModelResonance(Signal.NO_SIGNAL);

        int i = 0;
        testModel.run();
        while (!testModel.isConcUp()) {
            i++;
            Logger.getAnonymousLogger().log(Level.SEVERE, String.valueOf(testModel.getValue()));
            Utils.sleep(9);
            if (i == 5) {
                testModel.stop();
                Utils.sleep(500);
                testModel.run();
            }
        }

        testModel.reverse();
        while (!testModel.isConcDown()) {
            Logger.getAnonymousLogger().log(Level.SEVERE, String.valueOf(testModel.getValue()));
            Utils.sleep(9);
        }

        testModel.stop();
    }
}
