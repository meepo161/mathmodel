package ru.avem.mathModel.model;

import ru.avem.mathModel.utils.Utils;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;

import static java.lang.Thread.sleep;

public class MathModelResonance {
    private ArrayList<Float> dots = new ArrayList<>();
    private ListIterator<Float> iterator;
    private volatile float value;
    private volatile boolean isConcUp = false;
    private volatile boolean isConcDown = true;
    private volatile Mode mode = Mode.STOP;
    private volatile boolean isNeedRefresh = true;

    public MathModelResonance(Signal signal) {
        generate(signal);
        thread.start();
    }

    private Thread thread = new Thread(() -> {
        while (isNeedRefresh) {
            switch (mode) {
                case FORWARD:
                    if (iterator.hasNext()) {
                        isConcUp = false;
                        Float next = iterator.next();
                        value = randFloat(next * 0.9f, next * 1.1f);
                    } else {
                        System.out.println("Верхний концевик");
                        mode = Mode.STOP;
                        isConcUp = true;
                        isConcDown = false;
                    }
                    break;
                case REVERSE:
                    if (iterator.hasPrevious()) {
                        isConcDown = false;
                        Float previous = iterator.previous();
                        value = randFloat(previous * 0.9f, previous * 1.1f);
                    } else {
                        System.out.println("Нижний концевик");
                        mode = Mode.STOP;
                        isConcDown = true;
                        isConcUp = false;
                    }
                    break;
                case STOP:
                    break;
            }
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });

    private void generate(Signal signal) {
        float i = 1;
        switch (signal) {
            case PERFECT:
                createPerfrectDots(i, 500, 10);
                break;
            case GOOD:
                createDots(i, 500, 10, 1);
                break;
            case BAD:
                createDots(i, 500, 10, 5);
                break;
            case NO_SIGNAL:
                createDots(i, 500, 10, 8);
                break;
        }
        iterator = dots.listIterator();

    }

    private void createPerfrectDots(float i, int numberOfDots, int speed) {
        while (i <= numberOfDots) {
            i += speed;
            dots.add(i);
        }
        while (i >= numberOfDots) {
            i -= speed;
            dots.add(i);
        }
    }

    private void createDots(float i, int numberOfDots, int speed, int randomEnd) {
        while (i <= numberOfDots) {
            i += speed + randInt(0, randomEnd);
            dots.add(i);
        }
        while (i >= numberOfDots) {
            i -= speed - randInt(0, randomEnd);
            dots.add(i);
        }
    }

    public void run() {
        System.out.println("Поехали вверх");
        Utils.sleep(20);
        mode = Mode.FORWARD;
    }

    public void reverse() {
        System.out.println("Поехали вниз");
        Utils.sleep(20);
        mode = Mode.REVERSE;
    }

    public void stop() {
        System.out.println("Моя остановочка");
        Utils.sleep(20);
        mode = Mode.STOP;
    }

    private static int randInt(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    private float randFloat(float min, float max) {
        Random random = new Random(System.currentTimeMillis());
        return random.nextInt((int) (((max * 1000 - min * 1000)) / 1000) + 1) + min;
    }

    public float getValue() {
        return value;
    }

    public boolean isConcUp() {
        return isConcUp;
    }

    public boolean isConcDown() {
        return isConcDown;
    }

    public void destroy() {
        isNeedRefresh = false;
    }
}
