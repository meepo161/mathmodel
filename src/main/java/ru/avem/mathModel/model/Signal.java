package ru.avem.mathModel.model;

public enum Signal {
    PERFECT,
    GOOD,
    BAD,
    NO_SIGNAL
}
