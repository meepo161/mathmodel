package ru.avem.mathModel.model;

public enum Mode {
    FORWARD,
    REVERSE,
    STOP
}